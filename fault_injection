#!/usr/bin/env python
# coding: utf-8

# In[1]:


get_ipython().run_line_magic('matplotlib', 'inline')
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers, models
from tensorflow.keras.models import load_model
import os
import numpy as np
import matplotlib.pyplot as plt
from jupyterthemes import jtplot
jtplot.reset()
cifar10 = keras.datasets.cifar10
mnist = keras.datasets.mnist

(trx_mnist, try_mnist), (tex_mnist, tey_mnist) = mnist.load_data()
trx_mnist_norm = trx_mnist.reshape((60000,  28, 28,1))/255.0
tex_mnist_norm = tex_mnist.reshape((10000,  28, 28,1))/255.0

(train_images, train_labels), (test_images, test_labels) = cifar10.load_data()
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']
train_images = train_images.astype('float32')
test_images = test_images.astype('float32')

train_images_norm_c = train_images / 255.0
test_images_norm_c = test_images / 255.0

#####################################################################
def grayscale(data, dtype='float32'):
    # luma coding weighted average in video systems
    r, g, b = np.asarray(.3, dtype=dtype), np.asarray(.59, dtype=dtype), np.asarray(.11, dtype=dtype)
    rst = r * data[:, :, :, 0] + g * data[:, :, :, 1] + b * data[:, :, :, 2]
    # add channel dimension
    rst = np.expand_dims(rst, axis=3)
    return rst
#####################################################################

train_images_norm = grayscale(train_images_norm_c)
test_images_norm = grayscale(test_images_norm_c)


# In[2]:


chc = np.random.randint(0,10000,10)
fig, ax = plt.subplots(1,10, figsize=(8,0.8))
for idx, ch in enumerate(chc):
    #print(idx//5, idx%5)
    print(np.size(trx_mnist_norm[ch]))
    ax[idx%10].imshow(np.squeeze(trx_mnist_norm[ch]),cmap='Greys_r')
    
    ax[idx%10].set_xticks([])
    ax[idx%10].set_yticks([])
plt.tight_layout()
plt.show()
plt.savefig('mnist.pdf', dpi=1600, pad_inches=0)


# In[3]:


from keras.utils import plot_model
from keras.models import Model
from keras.layers import Input
from keras.layers import Dense, Activation, BatchNormalization
from keras.layers import Dropout, UpSampling2D, ZeroPadding2D
from keras.layers import Flatten, Cropping1D
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPooling2D, AveragePooling2D
from keras.engine.topology import Layer
from keras.layers import Lambda, Input
import tensorflow as tf

    
class Hadamard(Layer):

    def __init__(self, **kwargs):
        #self.output_dim = output_dim
        #super(MyLayer, self).__init__(**kwargs)
        super(Hadamard, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        self.kernel = self.add_weight(name='kernel', 
                                      #shape=(input_shape[1], self.output_dim),
                                      shape=(1,) + input_shape[1:],
                                      initializer='uniform',                                      
                                      trainable=True)
        super(Hadamard, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        #print(x.shape, self.kernel.shape)
        return x * tf.exp(2.0*np.pi*1j*tf.keras.backend.cast(self.kernel, dtype='complex64')           )

    def compute_output_shape(self, input_shape):
        #print(input_shape)
        return input_shape

    




class NHadamard(Layer):

    def __init__(self, **kwargs):
        #self.output_dim = output_dim
        #super(MyLayer, self).__init__(**kwargs)
        super(NHadamard, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        self.kernel = self.add_weight(name='kernel', 
                                      #shape=(input_shape[1], self.output_dim),
                                      shape=(1,) + input_shape[1:],
                                      initializer='uniform',                                      
                                      trainable=True)
        super(NHadamard, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        #print(x.shape, self.kernel.shape)
        linpart =  x * tf.exp(2.0*np.pi*1j*tf.keras.backend.cast(self.kernel, dtype='complex64')           )
        xsq = tf.math.abs(x)**2
        Ifull = 2
        return linpart*tf.exp(2.0*np.pi*1j*tf.keras.backend.cast(xsq/Ifull, dtype='complex64'))

    def compute_output_shape(self, input_shape):
        #print(input_shape)
        return input_shape   
    
    


def propagate(x):
    N = 256
    d = 40 # plate spacing
    lam = 1    # wavelength 
    P = 0.5  # pitch of metasurface
    df = 1.0/(N*P)
    k = np.pi*2.0/lam
    

    def phase(i,j):
        i -= N//2
        j -= N//2
        return ((i*df)*(i*df)+(j*df)*(j*df))
    ph  = np.fromfunction(phase,shape=(N,N),dtype=np.float32)
#     H = np.expand_dims(np.fft.fftshift(np.exp(1.0j*k*d)*np.exp(-1.0j*lam*np.pi*d*ph)), axis=2)
    H = np.fft.fftshift(np.exp(1.0j*k*d)*np.exp(-1.0j*lam*np.pi*d*ph))
    
    x = tf.squeeze(x, axis=3)
    x2 = (tf.keras.backend.cast(x, dtype='complex64'))
    Fx = tf.fft2d(x2)*H
    
    out = tf.ifft2d(Fx)
    return tf.expand_dims(out, axis=3)

def oeconvert(x):
    return np.abs(x)**2

# photodetector pooling layer and o/E conversion
def pdpooler(x):
    x2 = (tf.math.abs(x)**2)
    N = 256
    boxes=[[ 51, 102,  51, 102], #0
    [103, 154,  51, 102],  #1
    [155, 206,  51, 102],   #2
    [ 24,  75, 103, 154],   #3
    [ 76, 127, 103, 154],   #4
    [128, 179, 103, 154],   #5
    [180, 231, 103, 154],   #6
    [ 51, 102, 155, 206],    #7
    [103, 154, 155, 206],   #8
    [155, 206, 155, 206]]   #9
 
        
#     boxes=[[120, 170, 120, 170], #0
#     [120, 170, 240, 290],  #1
#     [120, 170, 360, 410],   #2
#     [220, 270, 120, 170],   #3
#     [220, 270, 200, 250],   #4
#     [220, 270, 280, 330],   #5
#     [220, 270, 360, 410],   #6
#     [320, 370, 120, 170],    #7
#     [320, 370, 240, 290],   #8
#     [320, 370, 360, 410]]   #9
    
    
    #region = tf.math.reduce_max(x[:,3:5,3:5,:], axis=[1,2])
    region = tf.stack([ 0.2*tf.squeeze(tf.math.reduce_sum(x2[:, b[0]:b[1], b[2]:b[3],:], axis=[1,2]), axis=1)  for b in boxes])
    #region = tf.stack([ 0.001  for b in boxes], axis=1)
    #reg = tf.stack( [ np.amax(x2[:, 3:5, 3:5,:], axis=[1,2])  for b in boxes], axis=1)
    
    return tf.transpose(region)

class Corr4f(Layer):

    def __init__(self, **kwargs):
        #self.output_dim = output_dim
        #super(MyLayer, self).__init__(**kwargs)
        super(Corr4f, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        self.kernel = self.add_weight(name='kernel', 
                                      #shape=(input_shape[1], self.output_dim),
                                      shape=(1,) + input_shape[1:],
                                      initializer='uniform',                                      
                                      trainable=True)
        super(Corr4f, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        #print(x.shape, self.kernel.shape)
        x3 = tf.squeeze(x, axis=3)
        x2 = (tf.keras.backend.cast(x3, dtype='complex64'))
        Fx = tf.fft2d(x2)
        plate = Fx * tf.squeeze(tf.exp(2.0*np.pi*1j*tf.keras.backend.cast(self.kernel, dtype='complex64') ), axis=3)
        out = tf.expand_dims(tf.ifft2d(plate), axis =3)
        xsq = tf.math.abs(out)**2
        Ifull = 2
        out = out*tf.exp(2.0*np.pi*1j*tf.keras.backend.cast(xsq/Ifull, dtype='complex64'))
        return out

    def compute_output_shape(self, input_shape):
        #print(input_shape)
        return input_shape

def satabs(x):
    #print(x.shape, self.kernel.shape)
    S = 0.1
    #fac = 1.0/(1.0 - S)
    x = tf.keras.backend.cast(x, dtype='complex64')
    il = tf.math.abs(x*x)
    iout = 0.5*(tf.math.abs(il - S) + il - S + 0.0000001    )
    ampo = tf.math.sqrt(iout)
    angpart = tf.exp(1j*tf.keras.backend.cast( tf.math.angle(x), dtype='complex64')               )
    xo = angpart*tf.keras.backend.cast(ampo, dtype='complex64')
    return xo


# In[4]:


class Err_Hadamard(Layer):

    def __init__(self, **kwargs):
        #self.output_dim = output_dim
        #super(MyLayer, self).__init__(**kwargs)
        super(Err_Hadamard, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        self.kernel = self.add_weight(name='kernel', 
                                      #shape=(input_shape[1], self.output_dim),
                                      shape=(1,) + input_shape[1:],
                                      initializer='uniform',                                      
                                      trainable=True)
        super(Err_Hadamard, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        error = tf.random.normal(self.kernel.shape, mean=0.0, stddev=0.10, dtype=tf.dtypes.float32, seed=None, name=None)
        print(error.shape, self.kernel.shape)
        self.kernel =  error + self.kernel
        return x * tf.exp(2.0*np.pi*1j*tf.keras.backend.cast(self.kernel, dtype='complex64')           )

    def compute_output_shape(self, input_shape):
        #print(input_shape)
        return input_shape


# In[5]:


# iput = Input(shape=(32,32,1))
# upsamp = UpSampling2D(size=2)(iput)
# padded = ZeroPadding2D(padding=96)(upsamp)
# prop1 = Lambda(propagate)(padded)
# hada1 = Err_Hadamard()(prop1)


# In[6]:


#plt.figure(figsize=(3,3))
ims = train_images_norm[1]
# with tf.Session() as sess:
#     imsn = satabs(ims).eval()
# plt.subplot(121)
# plt.imshow( np.squeeze(np.abs( ims[1]) ) )
# plt.subplot(122)
# plt.imshow( np.squeeze(np.abs( imsn[1]) ) )
# plt.show()
# x00 = tf.roll(x, shift=[0,1,1,0], axis=[0,1,2,3])


# In[10]:


multiplier, sf = 1000, 0.20
kappa, dpr = 1, 0
xs,ys = 0,0
iput = Input(shape=(28,28,1))
upsamp = UpSampling2D(size=2)(iput)
padded = ZeroPadding2D(padding=100)(upsamp)
prop1 = Lambda(propagate)(padded)
hada1 = Faulty_Hadamard(multiplier, sf, kappa, dpr, xs, ys)(prop1)
prop2 = Lambda(propagate)(hada1)
pdo = Lambda(pdpooler)(prop2)
oput = Activation('softmax')(pdo)

model_f = Model(inputs = iput, outputs = oput)
model_f.summary()


# In[11]:


model_f.compile(optimizer = 'nadam',
               loss = 'sparse_categorical_crossentropy',
               metrics=['accuracy'])
hstry3 = model_f.fit(trx_mnist_norm, try_mnist, epochs = 60,
                    batch_size=128, shuffle=True,
                    validation_data=(tex_mnist_norm, tey_mnist))


# In[11]:


fig = plt.figure(figsize = (15,7), dpi = 100)

ax1 = fig.add_subplot(1,2,1) 
ax1.plot(hstry3.history['accuracy'], linewidth = 3)
# ax1.plot(silicon[:,0], silicon[:,1], linewidth = 3, color = 'orange')
# ax1.set_ylim(0,0.6)

ax2 = ax1.twinx()
ax2.plot(hstry3.history['loss'], linewidth = 3, color = 'crimson')
# ax2.set_ylim(0,13)

ax3 = fig.add_subplot(1,2,2) 
ax3.plot(hstry3.history['val_accuracy'], linewidth = 3)
# ax1.plot(silicon[:,0], silicon[:,1], linewidth = 3, color = 'orange')
# ax3.set_ylim(0,0.6)

ax4 = ax3.twinx()
ax4.plot(hstry3.history['val_loss'], linewidth = 3, color = 'crimson')
# ax4.set_ylim(0,13)
plt.show()


# In[12]:


w1 = np.array(model_f.layers[4].get_weights())
w1 = np.reshape(w1,(256,256))
plt.imshow(w1, cmap='bwr')
plt.colorbar()
plt.show()
print(w1.shape)
np.savetxt('MNIST_1H.txt', w1)


# In[9]:


#Fault test
class Faulty_Hadamard(Layer):

    def __init__(self, multiplier=1000, sf=0.0, kappa = 1, dpr = 0, xs =0, ys = 0, **kwargs):
        #self.output_dim = output_dim
        #super(MyLayer, self).__init__(**kwargs)
        self.multiplier = multiplier
        self.sf = sf
        self.kappa = kappa
        self.dpr = dpr
        self.xs = xs
        self.ys = ys
        super(Faulty_Hadamard, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        self.kernel = self.add_weight(name='kernel', 
                                      #shape=(input_shape[1], self.output_dim),
                                      shape=(1,) + input_shape[1:],
                                      initializer='uniform',                                      
                                      trainable=True)
        super(Faulty_Hadamard, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        print(self.multiplier, self.sf, self.kappa, self.dpr, self.xs, self.ys)

        multiplier = tf.constant(self.multiplier, dtype='float32')
#         print(multiplier)

        x_rounded_NOT_differentiable = tf.round(self.kernel * multiplier) /multiplier
        y = (self.kernel - (tf.stop_gradient(self.kernel) - x_rounded_NOT_differentiable))
        y = y%1
        error = tf.random.normal(self.kernel.shape, mean=0.0, stddev=self.sf, dtype=tf.dtypes.float32, seed=None, name=None)
        #print(error.shape, self.kernel.shape)
        y = (y + error)%1
        self.kernel = y
        y00 = tf.roll(y, shift=[0,1,1,0], axis=[0,1,2,3])
        y01 = tf.roll(y, shift=[0,1,0,0], axis=[0,1,2,3])
        y02 = tf.roll(y, shift=[0,1,-1,0], axis=[0,1,2,3])
        
        y10 = tf.roll(y, shift=[0,0,1,0], axis=[0,1,2,3])
        y11 = tf.roll(y, shift=[0,0,0,0], axis=[0,1,2,3])
        y12 = tf.roll(y, shift=[0,0,-1,0], axis=[0,1,2,3])
        
        y20 = tf.roll(y, shift=[0,-1,1,0], axis=[0,1,2,3])
        y21 = tf.roll(y, shift=[0,-1,0,0], axis=[0,1,2,3])
        y22 = tf.roll(y, shift=[0,-1,-1,0], axis=[0,1,2,3])
        
        if (self.xs >= 0 and self.ys >= 0):
            y_res = (1-abs(self.xs))*(1-abs(self.ys))*y11 + (1-abs(self.xs))*abs(self.ys)*y21 + abs(self.xs)*(1-abs(self.ys))*y12 + abs(self.xs)*abs(self.ys)*y22
            
        elif (self.xs >= 0 and self.ys < 0):
            y_res = (1-abs(self.xs))*(1-abs(self.ys))*y11 + (1-abs(self.xs))*abs(self.ys)*y01 + abs(self.xs)*(1-abs(self.ys))*y12 + abs(self.xs)*abs(self.ys)*y02
        
        elif (self.xs < 0 and self.ys >= 0):
            y_res = (1-abs(self.xs))*(1-abs(self.ys))*y11 + (1-abs(self.xs))*abs(self.ys)*y21 + abs(self.xs)*(1-abs(self.ys))*y10 + abs(self.xs)*abs(self.ys)*y20
    
        elif (self.xs < 0 and self.ys < 0):
            y_res = (1-abs(self.xs))*(1-abs(self.ys))*y11 + (1-abs(self.xs))*abs(self.ys)*y01 + abs(self.xs)*(1-abs(self.ys))*y10 + abs(self.xs)*abs(self.ys)*y00        


        #y = tf.round(self.kernel * multiplier) / multiplier
        
        err_amp = np.random.uniform(self.kappa,1,size=256*256)
        err_id = np.random.randint(256*256, size=(int(256*256*self.dpr*100//10000)))
        err_amp[err_id] = 0
        err_amp = np.reshape(err_amp, (1,256,256,1))
        
        return x * err_amp * tf.exp(2.0*np.pi*1j*tf.keras.backend.cast(y_res, dtype='complex64')           )
        #return x * tf.exp(2.0*np.pi*1j*tf.keras.backend.cast(self.kernel, dtype='complex64')           )

    def compute_output_shape(self, input_shape):
        #print(input_shape)
        return input_shape




def propagate1(x):
    N = 256
    d = 40 # plate spacing
    lam = 1    # wavelength 
    P = 0.5  # pitch of metasurface
    df = 1.0/(N*P)
    k = np.pi*2.0/lam
    

    def phase(i,j):
        i -= N//2
        j -= N//2
        return ((i*df)*(i*df)+(j*df)*(j*df))
    ph  = np.fromfunction(phase,shape=(N,N),dtype=np.float32)
#     H = np.expand_dims(np.fft.fftshift(np.exp(1.0j*k*d)*np.exp(-1.0j*lam*np.pi*d*ph)), axis=2)
    H = np.fft.fftshift(np.exp(1.0j*k*d)*np.exp(-1.0j*lam*np.pi*d*ph))
    
    x = tf.squeeze(x, axis=3)
    x2 = (tf.keras.backend.cast(x, dtype='complex64'))
    Fx = tf.fft2d(x2)*H
    
    out = tf.ifft2d(Fx)
    return tf.expand_dims(out, axis=3)

def propagate2(x):
    N = 256
    d = 40 # plate spacing
    lam = 1    # wavelength 
    P = 0.5  # pitch of metasurface
    df = 1.0/(N*P)
    k = np.pi*2.0/lam


    def phase(i,j):
        i -= N//2
        j -= N//2
        return ((i*df)*(i*df)+(j*df)*(j*df))
    ph  = np.fromfunction(phase,shape=(N,N),dtype=np.float32)
#     H = np.expand_dims(np.fft.fftshift(np.exp(1.0j*k*d)*np.exp(-1.0j*lam*np.pi*d*ph)), axis=2)
    H = np.fft.fftshift(np.exp(1.0j*k*d)*np.exp(-1.0j*lam*np.pi*d*ph))
    
    x = tf.squeeze(x, axis=3)
    
    ipn = 0.2    
    ip_noise = np.random.normal(0, ipn, size = (256,256))
    
#     ip_noise = ip_noise*(tf.math.reduce_max(x) - tf.math.reduce_min(x))
    
    x = x + ip_noise
    
    x2 = (tf.keras.backend.cast(x, dtype='complex64'))
    Fx = tf.fft2d(x2)*H
    
    out = tf.ifft2d(Fx)
    return tf.expand_dims(out, axis=3)


def pdpooler_noisy(x):
#     print('hello', x.shape)
    x2 = (tf.math.abs(x)**2)
    N = 256
    boxes=[[ 51, 102,  51, 102], #0
    [103, 154,  51, 102],  #1
    [155, 206,  51, 102],   #2
    [ 24,  75, 103, 154],   #3
    [ 76, 127, 103, 154],   #4
    [128, 179, 103, 154],   #5
    [180, 231, 103, 154],   #6
    [ 51, 102, 155, 206],    #7
    [103, 154, 155, 206],   #8
    [155, 206, 155, 206]]   #9
    
    boxes = np.array(boxes) + 1

        
#     boxes=[[120, 170, 120, 170], #0
#     [120, 170, 240, 290],  #1
#     [120, 170, 360, 410],   #2
#     [220, 270, 120, 170],   #3
#     [220, 270, 200, 250],   #4
#     [220, 270, 280, 330],   #5
#     [220, 270, 360, 410],   #6
#     [320, 370, 120, 170],    #7
#     [320, 370, 240, 290],   #8
#     [320, 370, 360, 410]]   #9
    
    
    #region = tf.math.reduce_max(x[:,3:5,3:5,:], axis=[1,2])
    region = tf.stack([ 0.2*tf.squeeze(tf.math.reduce_sum(x2[:, b[0]:b[1], b[2]:b[3],:], axis=[1,2]), axis=1)  for b in boxes])
    region2 = region + np.random.normal(0, 0, size=(10,1))
    #region = tf.stack([ 0.001  for b in boxes], axis=1)
    #reg = tf.stack( [ np.amax(x2[:, 3:5, 3:5,:], axis=[1,2])  for b in boxes], axis=1)
    
    return tf.transpose(region2)




# In[ ]:


#Full fault model

# multiplier and sf calculation


multiplier = 100
sf = 0.025

kappa = 0.9
dpr = 1.25

xs = 0
ys = 0
        
faulty_response = np.array([])

for i in range (100):
    xs = np.random.uniform(-0.2, 0.2)
    ys = np.random.uniform(-0.2, 0.2)
    
    print(i, multiplier, sf, kappa, dpr, xs, ys)
    
    iput = Input(shape=(32,32,1))
    upsamp = UpSampling2D(size=2)(iput)
    padded = ZeroPadding2D(padding=97)(upsamp)

    prop1 = Lambda(propagate2)(padded)
    hada1 = Faulty_Hadamard(xs, ys, multiplier, sf, kappa, dpr)(prop1)
    prop2 = Lambda(propagate1)(hada1)
    pdo = Lambda(pdpooler_noisy)(prop2)
    oput = Activation('softmax')(pdo)

    model_fault = Model(inputs = iput, outputs = oput)

    w_f = np.loadtxt('1Hadamard_weights.txt')
    w_f = np.reshape(w_f, (1,1,256,256,1))

    model_fault.layers[4].set_weights(w_f)
    preds1 = np.reshape(test_labels, 10000)
    preds2 = np.argmax(model_fault.predict(test_images_norm), axis=1)
    temp1 = sum(preds1 == preds2)
    fault_ts = temp1/10000
    faulty_response = np.append(faulty_response,fault_ts)
    print(fault_ts)
with open ('allfault.txt', 'a') as ress:
    ress.write(str(faulty_response))

print(np.amin(faulty_response), np.amax(faulty_response))
print(sf, multiplier, faulty_response)


# In[13]:


# multiplier and sf calculation
xs1 = np.random.uniform(-0.3, 0.3, size = (10))
ys1 = np.random.uniform(-0.3, 0.3, size = (10))

multiplier = 1000
sf = 0
kappa = 1
dpr = 0
xs, ys = 0,0

mul1 = [1000]
sf1 = np.arange(0, 0.31, 0.01)

for mull in (mul1):
    for sff in (sf1):
        multiplier = mull
        sf = sff
        faulty_response = np.array([])        
        for i in range (10):
            print(i)
    
            iput = Input(shape=(28,28,1))
            upsamp = UpSampling2D(size=2)(iput)
            padded = ZeroPadding2D(padding=100)(upsamp)

            prop1 = Lambda(propagate)(padded)
            hada1 = Faulty_Hadamard(multiplier, sf, kappa, dpr, xs, ys)(prop1)
            prop2 = Lambda(propagate)(hada1)
            pdo = Lambda(pdpooler)(prop2)
            oput = Activation('softmax')(pdo)

            model_fault = Model(inputs = iput, outputs = oput)

            w_f = np.loadtxt('MNIST_1H.txt')
            w_f = np.reshape(w_f, (1,1,256,256,1))

            model_fault.layers[4].set_weights(w_f)
            preds1 = np.reshape(tey_mnist, 10000)
            preds2 = np.argmax(model_fault.predict(tex_mnist_norm), axis=1)
            temp1 = sum(preds1 == preds2)
            fault_ts = temp1/10000
            faulty_response = np.append(faulty_response,fault_ts)
            print(fault_ts)
        with open ('FIT_sf_20.txt', 'a') as ress:
            ress.write(str(faulty_response))
print(np.amin(faulty_response), np.amax(faulty_response))
print(sf, multiplier, faulty_response)


# In[ ]:


with open ('mnist_kappa_dpr.txt', 'a') as ress:
    ress.write(str(kappaa) + '\t' + str(dprr) + '\n' + str(faulty_response) + '\n' + '==========================')
    


# In[ ]:


xs = 0
ys = 0
multiplier = 1000
sf = 0

kappa1 = [0.8, 0.9]
dpr1 = [10,20,40,80]

xs1 = [-0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
ys1 = [-0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

# for xss in (xs1):
#     for yss in (ys1):
#         xs = xss
#         ys = yss
#         print(xs, ys)
iput = Input(shape=(32,32,1))
upsamp = UpSampling2D(size=2)(iput)
padded = ZeroPadding2D(padding=97)(upsamp)
prop1 = Lambda(propagate1)(padded)
hada1 = Faulty_Hadamard(xs, ys, multiplier, sf, kappa, dpr)(prop1)
prop2 = Lambda(propagate1)(hada1)
pdo = Lambda(pdpooler_noisy)(prop2)
oput = Activation('softmax')(pdo)
model_fault = Model(inputs = iput, outputs = oput)
w_f = np.loadtxt('1Hadamard_weights.txt')
w_f = np.reshape(w_f, (1,1,256,256,1))
model_fault.layers[4].set_weights(w_f)
preds1 = np.reshape(test_labels, 10000)
preds2 = np.argmax(model_fault.predict(test_images_norm), axis=1)
temp1 = sum(preds1 == preds2)
fault_ts = temp1/10000
print(fault_ts)
#         faulty_response = np.append(faulty_response,fault_ts)
        


# In[ ]:





# In[ ]:





# In[ ]:


kappa1 = [0.8, 0.9]
dpr1 = [10,20,40,80]

# multiplier and sf calculation
ys = 0
xs = 0
multiplier = 1000
sf = 0



for kappaa in (kappa1):
    for dprr in (dpr1):
        kappa = kappaa
        dpr = dprr

        faulty_response = np.array([])
        
        print(kappa, dpr)
        
        for i in range (100):
            print(i)
    
            iput = Input(shape=(32,32,1))
            upsamp = UpSampling2D(size=2)(iput)
            padded = ZeroPadding2D(padding=97)(upsamp)

            prop1 = Lambda(propagate1)(padded)
            hada1 = Faulty_Hadamard(xs, ys, multiplier, sf, kappa, dpr)(prop1)
            prop2 = Lambda(propagate1)(hada1)
            pdo = Lambda(pdpooler_noisy)(prop2)
            oput = Activation('softmax')(pdo)

            model_fault = Model(inputs = iput, outputs = oput)

            w_f = np.loadtxt('1Hadamard_weights.txt')
            w_f = np.reshape(w_f, (1,1,256,256,1))

            model_fault.layers[4].set_weights(w_f)
            preds1 = np.reshape(test_labels, 10000)
            preds2 = np.argmax(model_fault.predict(test_images_norm), axis=1)
            temp1 = sum(preds1 == preds2)
            fault_ts = temp1/10000
            faulty_response = np.append(faulty_response,fault_ts)
        with open ('mul_sf.txt', 'a') as ress:
            ress.write(str(kappa) + '\t' + str() + '\t' + str(faulty_response) + '\n')
print(np.amin(faulty_response), np.amax(faulty_response))
print(sf, multiplier, faulty_response)


# In[ ]:


from numpy import random
rnd=random.randint(100, size=(100))

print(test_labels[x])

xx = np.array([1,2])
y = np.append(xx, 3)
print(y)

# for i in range (5):
#     rnd = random.randint(0,1000)
#     print(rnd)

faulty_response = np.array([])

for i in range (10):

    rnd = random.randint(10000, size=(1000))

    preds1 = np.reshape(test_labels[rnd], 1000)
    preds2 = np.argmax(model_fault.predict(test_images_norm[rnd]), axis=1)
    temp1 = sum(preds1 == preds2)
    fault_ts = temp1/1000
    faulty_response = np.append(faulty_response,fault_ts)

print(np.amin(faulty_response), np.amax(faulty_response))

with open ('multiplier.txt', 'w') as mull:
    for elmt in (faulty_response):
        mull.write(str(elmt) + '\t')


# In[ ]:


print(-1.3%1)


# In[ ]:


int(258*258*2.5*100//10000)


# In[ ]:





# In[ ]:


# 2 linear phseplate - loss: 1.5202 - accuracy: 0.4775 - val_loss: 1.7130 - val_accuracy: 0.4048
w1 = np.array(model_f.layers[4].get_weights())


# In[ ]:


count = 0
Etr = []
Ets = []

while count < 100:
    wgt_1 = w1
    err_1 = np.random.normal(0, 0.0, size=(256, 256))
    wgt_er1 = wgt_1 + np.reshape(err_1, (1, 1, 256, 256, 1))
    # plt.imshow(np.reshape(wgt_1, (256,256)), cmap='bwr')
    # plt.colorbar()
    # plt.show()

    wgt_2 = w2
    err_2 = np.random.normal(0, 0.0, size=(256, 256))
    wgt_er2 = wgt_2 + np.reshape(err_2, (1, 1, 256, 256, 1))

    # plt.imshow(np.reshape(wgt_2, (256,256)), cmap='bwr')
    # plt.colorbar()
    # plt.show()
    model_f.layers[4].set_weights(wgt_er1)
    model_f.layers[6].set_weights(wgt_er2)
    
    preds1 = np.reshape(test_labels, 10000)
    preds2 = np.argmax(model_f.predict(test_images_norm), axis=1)
    temp1 = sum(preds1 == preds2)
#     print(temp/len(test_labels))
    Ets.append(temp1/len(test_labels))
    
    preds3 = np.reshape(train_labels, 50000)
    preds4 = np.argmax(model_f.predict(train_images_norm), axis=1)
    temp2 = sum(preds3 == preds4)
    Etr.append(temp2/len(train_labels))    
    count = count + 1


# In[ ]:


np.amin(Etr)


# In[ ]:


model_upsmp = Model(inputs=iput, outputs=upsamp)
u = np.abs(model_upsmp.predict(test_images_norm))**2

model_padded = Model(inputs=iput, outputs=padded)
p = np.abs(model_padded.predict(test_images_norm))**2

model_prop1 = Model(inputs=iput, outputs=prop1)
p1 = np.abs(model_prop1.predict(test_images_norm))

model_hada1 = Model(inputs=iput, outputs=hada1)
h1 = np.abs(model_hada1.predict(test_images_norm))

model_prop2 = Model(inputs=iput, outputs=prop2)
p2 = np.abs(model_prop2.predict(test_images_norm))

model_pdo = Model(inputs=iput, outputs=pdo)
pd = np.abs(model_pdo.predict(test_images_norm))

pdsig = np.abs(model_f.predict(test_images_norm))


# In[ ]:


import matplotlib.patches as patches
idx = np.random.randint(10000)
idx = 9874
#plt.subplot(121)
fig, ax = plt.subplots(1,6)
ax[0].imshow(np.squeeze(test_images_norm[idx]), cmap='Greys_r', vmin=0, vmax=1)
ax[0].set_xticks([])
ax[0].set_yticks([])

ax[1].imshow(np.squeeze(np.squeeze(u[idx])), cmap='Greys_r', vmin=0, vmax=1)
ax[1].set_xticks([])
ax[1].set_yticks([])

ax[2].imshow(np.squeeze(np.squeeze(p[idx])), cmap='Greys_r', vmin=0, vmax=1)
ax[2].set_xticks([])
ax[2].set_yticks([])

ax[3].imshow(np.squeeze(np.squeeze(p1[idx])), cmap='Greys_r', vmin=0, vmax=1)
ax[3].set_xticks([])
ax[3].set_yticks([])

ax[4].imshow(np.squeeze(np.squeeze(h1[idx])), cmap='Greys_r', vmin=0, vmax=1)
ax[4].set_xticks([])
ax[4].set_yticks([])

ax[5].imshow(np.squeeze(np.squeeze(p2[idx])), cmap='Greys_r', vmin=0, vmax=1)
ax[5].set_xticks([])
ax[5].set_yticks([])

# ax[6].imshow(np.squeeze(np.squeeze(pd[idx])), cmap='Greys_r', vmin=0, vmax=1)
# ax[6].set_xticks([])
# ax[6].set_yticks([])

plt.show()

print(pdsig[idx])


# In[ ]:





# In[ ]:


def randabs(x):
    Kappa = 0.7
    x = tf.keras.backend.cast(x, dtype='complex64')
    abr = tf.keras.backend.cast(tf.random_uniform(shape=[256,256,1], minval=Kappa, maxval=1.0), dtype='complex64')
    return x*abr

iput = Input(shape=(32,32,1))
upsamp = UpSampling2D(size=2)(iput)
padded = ZeroPadding2D(padding=96)(upsamp)

prop1 = Lambda(propagate)(padded)
hada1 = NHadamard()(prop1)
hada1_abs = Lambda(randabs)(hada1)

prop2 = Lambda(propagate)(hada1_abs)
hada2 = NHadamard()(prop2)
hada2_abs = Lambda(randabs)(hada2)

prop3 = Lambda(propagate)(hada2_abs)
pdo = Lambda(pdpooler)(prop3)
oput = Activation('softmax')(pdo)

model_f = Model(inputs = iput, outputs = oput)
model_f.summary()


model_f.compile(optimizer='nadam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
hstry3 = model_f.fit(train_images_norm, train_labels, epochs=60, batch_size=128, shuffle=True,
           validation_data=(test_images_norm, test_labels))


# In[ ]:


w1.shape


# In[ ]:


abs(xs)


# In[13]:





# In[ ]:




